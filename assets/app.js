/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.css';

// start the Stimulus application
import './bootstrap';

// const routes = require('../public/js/fos_js_routes.json');
// import Routing from '../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router';
// Routing.setRoutingData(routes);
// // console.log(Routing);

// console.log(Routing.generate('question_index', {page: 1}, true)); // will prints in console /path/to/exposed_route

$('.glyphicon-edit').on('click', function () {
        var brandId = 'brand'
        var langIframe = 'langIframe';
        var iframeKey = 'iframeKey';
        var url = "https://127.0.0.1:8000/question/2/edit";
        $.ajax({
            type: 'post',
            url: url,
            data: {
                'brandId': brandId,
                'langIframe': langIframe,
                'key': iframeKey
            },
            success: function (responseData) {
                // $('#purchase_model_ajax').html(responseData);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
    });