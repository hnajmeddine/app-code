<?php

namespace App\Entity;

use App\Repository\QuestionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=QuestionRepository::class)
 */
class Question
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $label;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $zoneId;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $typeId;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $propertieId;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $parent;

    /**
     * @ORM\Column(type="boolean")
     */
    private $enabled;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="integer")
     */
    private $creationUserId;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $updateUserId;

    /**
     * @ORM\Column(type="datetime")
     */
    private $timestamp;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->timestamp = new \DateTime();
        $this->enabled   = true;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getZoneId(): ?int
    {
        return $this->zoneId;
    }

    public function setZoneId(?int $zoneId): self
    {
        $this->zoneId = $zoneId;

        return $this;
    }

    public function getTypeId(): ?int
    {
        return $this->typeId;
    }

    public function setTypeId(?int $typeId): self
    {
        $this->typeId = $typeId;

        return $this;
    }

    public function getPropertieId(): ?int
    {
        return $this->propertieId;
    }

    public function setPropertieId(?int $propertieId): self
    {
        $this->propertieId = $propertieId;

        return $this;
    }

    public function getParent(): ?string
    {
        return $this->parent;
    }

    public function setParent(?string $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    public function getEnabled(): ?bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getCreationUserId(): ?int
    {
        return $this->creationUserId;
    }

    public function setCreationUserId(int $creationUserId): self
    {
        $this->creationUserId = $creationUserId;

        return $this;
    }

    public function getUpdateUserId(): ?int
    {
        return $this->updateUserId;
    }

    public function setUpdateUserId(int $updateUserId): self
    {
        $this->updateUserId = $updateUserId;

        return $this;
    }

    public function getTimestamp(): ?\DateTimeInterface
    {
        return $this->timestamp;
    }

    public function setTimestamp(\DateTimeInterface $timestamp): self
    {
        $this->timestamp = $timestamp;

        return $this;
    }
}
