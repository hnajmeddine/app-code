<?php

namespace App\Entity;

use App\Repository\QuestionChoiceRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=QuestionChoiceRepository::class)
 */
class QuestionChoice
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $label;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $addPicture;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $customCost;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $hasCost;

    /**
     * @ORM\Column(type="integer")
     */
    private $questionId;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $reconditionningId;

    /**
     * @ORM\Column(type="datetime")
     */
    private $timestamp;

    public function __construct()
    {
        $this->timestamp = true;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getAddPicture(): ?bool
    {
        return $this->addPicture;
    }

    public function setAddPicture(?bool $addPicture): self
    {
        $this->addPicture = $addPicture;

        return $this;
    }

    public function getCustomCost(): ?bool
    {
        return $this->customCost;
    }

    public function setCustomCost(?bool $customCost): self
    {
        $this->customCost = $customCost;

        return $this;
    }

    public function getHasCost(): ?bool
    {
        return $this->hasCost;
    }

    public function setHasCost(?bool $hasCost): self
    {
        $this->hasCost = $hasCost;

        return $this;
    }

    public function getQuestionId(): ?int
    {
        return $this->questionId;
    }

    public function setQuestionId(int $questionId): self
    {
        $this->questionId = $questionId;

        return $this;
    }

    public function getReconditionningId(): ?int
    {
        return $this->reconditionningId;
    }

    public function setReconditionningId(?int $reconditionningId): self
    {
        $this->reconditionningId = $reconditionningId;

        return $this;
    }

    public function getTimestamp(): ?\DateTimeInterface
    {
        return $this->timestamp;
    }

    public function setTimestamp(\DateTimeInterface $timestamp): self
    {
        $this->timestamp = $timestamp;

        return $this;
    }
}
