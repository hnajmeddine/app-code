<?php

namespace App\Repository;

use App\Entity\QuestionZone;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method QuestionZone|null find($id, $lockMode = null, $lockVersion = null)
 * @method QuestionZone|null findOneBy(array $criteria, array $orderBy = null)
 * @method QuestionZone[]    findAll()
 * @method QuestionZone[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class QuestionZoneRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, QuestionZone::class);
    }

    // /**
    //  * @return QuestionZone[] Returns an array of QuestionZone objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('q.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?QuestionZone
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
