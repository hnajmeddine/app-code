<?php

namespace App\Repository;

use App\Entity\QuestionProperties;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method QuestionProperties|null find($id, $lockMode = null, $lockVersion = null)
 * @method QuestionProperties|null findOneBy(array $criteria, array $orderBy = null)
 * @method QuestionProperties[]    findAll()
 * @method QuestionProperties[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class QuestionPropertiesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, QuestionProperties::class);
    }

    // /**
    //  * @return QuestionProperties[] Returns an array of QuestionProperties objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('q.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?QuestionProperties
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
